import { useState, useEffect, useRef } from "react"
import { GoChevronDown, GoChevronUp } from "react-icons/go"
import Panel from './Panel'

function Dropdown({ options, value, onChange }) {

  const [showMenu, setShowMenu] = useState(false)
  const divEl = useRef()

  useEffect(() => {
    const handler = (event) => {

      if (!divEl.current) {
        return
      }

      if (!divEl.current.contains(event.target)) {
        setShowMenu(false)
      }
    }

    document.addEventListener('click', handler, true)

    return () => {
      document.removeEventListener('click', handler)
    }
  }, [])

  const handleMenuToggle = () => {
    setShowMenu((currentShowMenu) => !currentShowMenu)
  }

  const handleOptionSelect = (selectedOption) => {
    setShowMenu(false)
    onChange(selectedOption)
  }

  const renderedOptions = options.map((option, index) => {
    return (
      <div
        className="hover:bg-sky-100 rounded cursor-pointer p-1"
        key={option.value}
        onClick={() => handleOptionSelect(option)}
      >
        {option.label}
      </div>
    )
  })

  return (
    <div ref={divEl} className="w-48 relative">
      <Panel className="flex justify-between items-center cursor-pointer" onClick={handleMenuToggle}>
        {value?.label || 'Select an option...'}{showMenu ? <GoChevronUp /> : <GoChevronDown />}
      </Panel>
      {showMenu && <Panel className="absolute top-full">{renderedOptions}</Panel>}
    </div>
  )
}

export default Dropdown
