import { useState } from "react"
import { GoChevronDown, GoChevronLeft } from 'react-icons/go'

function Accordion({ items }) {

  const [expandedIndex, setExpandedIndex] = useState(-1)

  const handleClick = (nextIndex) => {
    setExpandedIndex((currentIndex) => {
      if (currentIndex === nextIndex) {
        return -1
      } else {
        return nextIndex
      }
    })
  }

  const renderedItems = items.map((item, index) => {

    const isExpanded = index === expandedIndex

    const icon = <span>{ isExpanded ? <GoChevronDown /> : <GoChevronLeft /> }</span>

    return (
      <div className='accordion' key={item.id} onClick={() => handleClick(index)}>
        <div className='flex justify-between p-2 bg-blue-900/80 border-b items-center text-white'>{item.title}{icon}</div>
        {isExpanded && <div className='border-b p-5 bg-white text-black p-2'>{item.content}</div>}
      </div>
    )
  })

  return (
    <div className='border-x border-t rounded'>{renderedItems}</div>
  )
}

export default Accordion
