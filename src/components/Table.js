import { Fragment } from "react"

function Table({ config, data, keyFn }) {

  const renderedHeaders = config.map((column) => {
    if (column.header) {
      return <Fragment key={column.label}>{column.header()}</Fragment>
    }

    return (
      <th key={column.label}>
        {column.label}
      </th>
    )
  })

  const renderedRows = data.map((row) => {

    const renderedCells = config.map((col) => {
      return (
        <td className="p-2" key={col.label}>
          {col.render(row)}
        </td>
      )
    })

    return (
      <tr className="border-b" key={keyFn(row)}>
        {renderedCells}
      </tr>
    )
  })

  return (
    <table className="table-auto border-spacing-2">
      <thead className="border-b-2">
        <tr>
          {renderedHeaders}
        </tr>
      </thead>
      <tbody>
        {renderedRows}
      </tbody>
    </table>
  )
}

export default Table
