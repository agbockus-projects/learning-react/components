import Link from "./Link"

function Sidebar() {

  const links = [
    {
      title: 'Dropdown',
      path: '/'
    },
    {
      title: 'Accordion',
      path: '/accordion'
    },
    {
      title: 'Buttons',
      path: '/buttons'
    },
    {
      title: 'Modal',
      path: '/modal'
    },
    {
      title: 'Table',
      path: '/table'
    },
    {
      title: 'Counter',
      path: '/counter'
    }
  ]

  const renderedLinks = links.map((link) => {
    return (
      <Link
        key={link.title}
        to={link.path}
        className="mb-3"
        activeClassName="font-bold border-l-4 border-blue-500 pl-2"
      >
        {link.title}
      </Link>
    )
  })

  return (
    <div className="sticky top-0 overflow-y-scroll flex flex-col items-start">
      {renderedLinks}
    </div>
  )
}

export default Sidebar
