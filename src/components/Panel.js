import classNames from 'classnames'

function Panel({ children, className, ...rest }) {

  const concatClassNames = classNames(
    'border rounded p-3 shadow bg-white w-full',
    className
  )

  return (
    <div {...rest} className={concatClassNames}>
      {children}
    </div>
  )
}

export default Panel
