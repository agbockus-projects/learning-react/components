import { useState } from 'react'
import Dropdown from '../components/Dropdown'

function DropdownPage() {

  const [selected, setSelected] = useState(null)

  const options = [
    {
      label: 'Mild',
      value: 'mild'
    },
    {
      label: 'Spicy',
      value: 'spicy'
    },
    {
      label: 'Extra Spicy',
      value: 'extra_spicy'
    }
  ]

  const handleSelect = (selectedOption) => {
    setSelected(selectedOption)
  }

  return (
    <div className='flex'>
      <Dropdown options={options} value={selected} onChange={handleSelect} />
    </div>
  )
}

export default DropdownPage
