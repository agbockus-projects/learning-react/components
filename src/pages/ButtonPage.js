import { GoBell, GoCloudDownload, GoDatabase } from 'react-icons/go'
import Button from '../components/Button'

function ButtonPage() {

  return (
    <div className='button-page'>
      <div>
        <Button success rounded outline><GoBell />Test 1</Button>
      </div>
      <div>
        <Button danger outline><GoCloudDownload />Test 2</Button>
      </div>
      <div>
        <Button warning><GoDatabase />Test 3</Button>
      </div>
      <div>
        <Button secondary outline>Test 4</Button>
      </div>
      <div>
        <Button secondary rounded>Test 5</Button>
      </div>
    </div>
  )
}

export default ButtonPage
