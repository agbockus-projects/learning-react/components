import Accordion from '../components/Accordion'

function AccordionPage() {

  const accordions = [
    {
      id: '5j43kl25',
      title: 'Can I use React on a project?',
      content: 'You can use it on whatever you would like!'
    },
    {
      id: 'j4lkp43',
      title: 'Can I use JavaScript on a project?',
      content: 'You can use it on whatever you would like!'
    },
    {
      id: 'uvcxz42',
      title: 'Can I use CSS on a project?',
      content: 'You can use it on whatever you would like!'
    }
  ]

  return (
    <div className='accordion-page'>
      <Accordion items={accordions} />
    </div>
  )
}

export default AccordionPage
